package org.home.schnake.fuzzy;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Configuration
@ConfigurationProperties(prefix = "fuzzy")
@Getter
@Setter
@Slf4j
@ToString
public class Config {
	private String templatePath;
	
	@PostConstruct
	public void init() {
		log.info("Config init: {}", this.toString());
	}
}
