package org.home.schnake.fuzzy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuzzyControllerNgApplication {

	public static void main(String[] args) {
		SpringApplication.run(FuzzyControllerNgApplication.class, args);
	}

}
