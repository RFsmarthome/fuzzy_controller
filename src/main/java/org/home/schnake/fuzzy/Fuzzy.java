package org.home.schnake.fuzzy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import lombok.extern.slf4j.Slf4j;
import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.rule.Variable;

@Slf4j
@Controller
public class Fuzzy {
	
	@Autowired
	Config config;

	public double calc(String template, double current, double past) {
		String templateFilename = config.getTemplatePath()+"/"+template+".fcl";
		
		log.debug("Load FCL {}", templateFilename);
		
		FIS fis = FIS.load(templateFilename, true);
		if(fis==null) {
			throw new RuntimeException("FCL file not found");
		}
		fis.setVariable("temperror", current);
		fis.setVariable("temperror_dt", past);
		
		fis.evaluate();
		
		Variable ventil = fis.getVariable("ventil");
		ventil.defuzzify();
		
		return ventil.getValue();
	}
}
