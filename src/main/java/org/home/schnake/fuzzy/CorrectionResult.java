package org.home.schnake.fuzzy;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Component
@Getter
@Setter
@ToString
public class CorrectionResult {
	private String template;
	private double current;
	private double past;
	private double valveCorrection;
}
