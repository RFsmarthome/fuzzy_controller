package org.home.schnake.fuzzy;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class TempApi {
	
	@Autowired
	Fuzzy fuzzy;
	
	@GetMapping("/valve_correction/{template}")
	public CorrectionResult calculate(@PathVariable(value="template")String template, @RequestParam(value="current")double current, @RequestParam(value="past")double past) {
		CorrectionResult result = new CorrectionResult();
		result.setTemplate(template);
		result.setCurrent(current);
		result.setPast(past);
		
		log.debug("Begin: {}", LocalDateTime.now());
		result.setValveCorrection(fuzzy.calc(template, current, past));
		log.debug("End: {}", LocalDateTime.now());
		return result;
	}
}
